# README #

This repository contains all Selenium Java End-to-End tests, that are published as a set of batch commands.

## Overview ##
* At the time of this writing, this repository contains 2 sample Selenium tests 
* These 2 tests are run using a TestNG xml file
* For now, the tests can only run in Chrome, but support for other browsers will be added later.
* All tests are configured to run in parallel to save time.
* Updated as of 29 May 2017

## Setup Instructions ##

### Pre-requisites ###

1. Your machine should have the latest JDK 8 version

### Download the Tests zip file from the BitBucket repository ###

After installing JDK 8 in your machine, you will have to download the test files from our BitBucket repository so you 
could run them in your local machine. Follow these steps:

1. Go to the [Downloads](https://bitbucket.org/rodel_calasagsag/perkbox-uk-selenium-tests/downloads/) page of this 
Bitbucket repository.
2. Download the `Download Tests.zip` file. This is the file where all the tests are contained. I will be updating this 
zip file as soon as I make new changes to the tests.
3. Unzip the file to any directory you wish to serve as the root directory.

### Running the tests in your local machine ###

The directory where you have unzipped the test files to will be your `~root` directory. Now, you're ready to run the 
tests. You just have to run 3 batch files in the exact order described below:

1. Run the batch file `~root/drivers/launch hub.bat`
2. Wait until you see the message in the console:
    > Selenium Grid hub is up and running
3. Run the batch file `~root/drivers/launch chrome.bat`
4. Wait for this message to appear before proceeding:
    > The node is registered to the hub and ready to use
5. Run the batch file `~root/run sample suite.bat`, which is the batch file for running all tests in parallel
6. After completing the tests, you should see the test results in the `~root/test-ouptut/index.html` file

