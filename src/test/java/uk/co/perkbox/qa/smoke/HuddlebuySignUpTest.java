package uk.co.perkbox.qa.smoke;

import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import uk.co.perkbox.qa.base.GlobalTestBase;
import uk.co.perkbox.qa.helpers.Browser;
import uk.co.perkbox.qa.pageobjects.HuddlebuyPage;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Use case 1.
 * <ol>
 * <li>Open <a href="https://www.perkbox.co.uk/goldcard/info/huddlebuytest">https://www.perkbox.co.uk/goldcard/info/huddlebuytest</a></li>
 * <li>Enter all the details on the form (Make sure you enter HELLO in “Secret code” field)</li>
 * <li>Submit the form and validate that the success message shows up</li>
 * </ol>
 */
public class HuddlebuySignUpTest extends GlobalTestBase {

    // domain values
    private String baseUrl;
    private String firstName;
    private String lastName;
    private String jobTitle;
    private String gender;
    private String birthDay;
    private String birthMonth;
    private String birthYear;
    private String email;
    private String mobile;
    private String secretCode;

    // page objects
    private HuddlebuyPage huddlebuyPage;

    @BeforeClass
    public void goToHuddlebuyPage(ITestContext context) {
        getDomainValues(context);
        driver.get(baseUrl);
        huddlebuyPage = new HuddlebuyPage(driver);

        assertThat(driver.getTitle(), equalTo(HuddlebuyPage.PAGE_TITLE));
        assertThat(driver.getCurrentUrl(), equalTo(baseUrl));
    }

    @Test
    public void user_submitsSignUpForm_successMessageShowsUp() {
        huddlebuyPage.typeFirstName(firstName)
                .typeLastName(lastName)
                .selectJobTitle(jobTitle)
                .selectGender(gender)
                .selectBirthDay(birthDay)
                .selectBirthMonth(birthMonth)
                .selectBirthYear(birthYear)
                .typeEmail(email)
                .typeMobile(mobile)
                .typeSecretCode(secretCode)
                .clickSubmit();

        assertThat(huddlebuyPage.showsSignUpSuccessMessage(), is(Boolean.TRUE));
    }

    private void getDomainValues(ITestContext context) {
        baseUrl = context.getCurrentXmlTest().getParameter("base-url");
        firstName = context.getCurrentXmlTest().getParameter("first-name");
        lastName = context.getCurrentXmlTest().getParameter("last-name");
        jobTitle = context.getCurrentXmlTest().getParameter("job-title");
        gender = context.getCurrentXmlTest().getParameter("gender");
        birthDay = context.getCurrentXmlTest().getParameter("birth-day");
        birthMonth = context.getCurrentXmlTest().getParameter("birth-month");
        birthYear = context.getCurrentXmlTest().getParameter("birth-year");
        email = context.getCurrentXmlTest().getParameter("email");
        mobile = context.getCurrentXmlTest().getParameter("mobile");
        secretCode = context.getCurrentXmlTest().getParameter("secret-code");
        randomizeEmail();
    }

    private void randomizeEmail() {
        int charLength = 6;
        String randomPart = browser.randomString(charLength) + "@";

        email = email.replace("@", randomPart);
        System.out.println("Randomly generated email = " + email);
    }
}
