package uk.co.perkbox.qa.smoke;

import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import uk.co.perkbox.qa.base.GlobalTestBase;
import uk.co.perkbox.qa.pageobjects.LoginPage;
import uk.co.perkbox.qa.pageobjects.deals.DealBlock;
import uk.co.perkbox.qa.pageobjects.deals.DealsPage;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Use Case 2:
 * <ol>
 * <li>Open <a href="https://www.perkbox.co.uk/goldcard/deals">https://www.perkbox.co.uk/goldcard/deals</a></li>
 * <li>Scroll down to find the deal “Save 10% on Microsoft Office 365”.
 * DO NOT use search feature to find the deal (https://screencast.com/t/9bL7KFlZiBl)</li>
 * <li>Click GET THIS PERK button</li>
 * <li>Validate that you are redirected to the login page</li>
 * </ol>
 */
public class GetPerkByScrollingTest extends GlobalTestBase {

    // domain values
    private String perkName;
    private String baseUrl;
    private String perkId;

    // page objects
    private DealsPage dealsPage;
    private DealBlock dealBlock;
    private LoginPage loginPage;

    @BeforeClass
    public void setupDomainAndNavigateToBaseUrl(ITestContext context) {
        getDomainValues(context);
        driver.get(baseUrl);
        dealsPage = new DealsPage(driver);

        assertThat(driver.getTitle(), equalTo(DealsPage.TITLE));
        assertThat(driver.getCurrentUrl(), equalTo(baseUrl));
    }

    @Test
    public void guest_selectsPerkByScrollingToIt_guestIsTakenToLoginPage() {
        dealBlock = dealsPage.scrollToPerk(perkName);
        perkId = dealBlock.getId();
        loginPage = dealBlock.clickGetThisPerkBtn();

        assertThat(driver.getCurrentUrl(), containsString(perkId));
        assertThat(loginPage.showsLoginForm(), is(Boolean.TRUE));
    }

    private void getDomainValues(ITestContext context) {
        perkName = context.getCurrentXmlTest().getParameter("perk-name");
        baseUrl = context.getCurrentXmlTest().getParameter("base-url");
    }
}
