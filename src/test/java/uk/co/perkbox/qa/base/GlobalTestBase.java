package uk.co.perkbox.qa.base;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import uk.co.perkbox.qa.helpers.Browser;

import java.lang.reflect.Method;
import java.net.MalformedURLException;

/**
 * Base class for all tests
 */
public class GlobalTestBase {

    protected String browserName;
    protected String hubUrl;
    protected WebDriver driver;
    protected Browser browser;

    @BeforeSuite
    public void beforeSuite() {
        // placeholder
    }

    @AfterSuite
    public void afterSuite() {
        // placeholder
    }

    @BeforeTest
    public void beforeTest(ITestContext context) throws MalformedURLException {
        getDomainValues(context);
        browser = new Browser(driver);

        driver = browser.setUpDriver(browserName, hubUrl);
    }

    @AfterTest
    public void afterTest() {
        browser.quit();
    }

    @BeforeMethod
    public void beforeMethod(Method method) {
        // placeholder
    }

    @AfterMethod
    public void afterMethod(ITestResult result, Method method) {
    }

    private void getDomainValues(ITestContext context) {
        browserName = context.getCurrentXmlTest().getParameter("browser-name");
        hubUrl = context.getCurrentXmlTest().getParameter("hub-url");
    }
}
