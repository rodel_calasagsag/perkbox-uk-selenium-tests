package uk.co.perkbox.qa.helpers;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Helper class for browser actions
 */
public class Browser {

    public enum Name {
        CHROME, FIREFOX, IE_11, EDGE;
    }

    private WebDriver driver;

    public Browser(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver setUpDriver(String strBrowserName, String hubUrl) throws MalformedURLException {
        DesiredCapabilities capabilities;
        Name bName = Name.valueOf(strBrowserName.toUpperCase());

        switch (bName) {
            case CHROME:
                capabilities = setupChromeCaps();
                break;

            default:
                capabilities = setupChromeCaps();
                break;
        }
        driver = new RemoteWebDriver(new URL(hubUrl), capabilities);
        driver.manage().window().maximize();

        return driver;
    }

    /**
     * Set up capabilities for chrome
     *
     * @return {@link DesiredCapabilities} of Chrome
     */
    private DesiredCapabilities setupChromeCaps() {
        String chromeDriverPath = System.getProperty("user.dir") + File.separator + "drivers" + File.separator + "ChromeDriver 2.29" + File.separator + "chromedriver.exe";

        System.setProperty("webdriver.chrome.driver", chromeDriverPath);

        return DesiredCapabilities.chrome();
    }

    /**
     * Pause the browser
     *
     * @param waitTimeInSec
     */
    public void pause(long waitTimeInSec) {
        try {
            new WebDriverWait(driver, waitTimeInSec).until(ExpectedConditions.titleIs(String.valueOf(Math.random())));
        } catch (Exception e) {
            // do nothing
        }
    }

    /**
     * Make the browser wait for <code>waitTime</code> seconds
     *
     * @param waitTime Number of seconds to wait
     * @return {@link WebDriverWait}
     */
    public WebDriverWait waitFor(long waitTime) {
        return new WebDriverWait(driver, waitTime);
    }

    /**
     * Run a javascript command
     *
     * @param cmd     Javascript command
     * @param element Webelement to be passed on to the javascript command
     */
    public void runScript(String cmd, WebElement element) {
        ((JavascriptExecutor) driver).executeScript(cmd, element);
    }

    /**
     * Generate a random string with a specified number of characters
     *
     * @param length Number of characters to be generated
     * @return Random string
     */
    public String randomString(int length) {
        long min = (long) Math.pow(10, --length);
        long max = (min * 10 - 1);
        long randomNum = (long) (Math.random() * (max - min) + min);
        return String.valueOf(randomNum);
    }

    public void quit() {
        if (driver != null) {
            driver.quit();
        }
    }

    public void runScript(String cmd) {
        System.out.println("Running script: " + cmd);
        ((JavascriptExecutor) driver).executeScript(cmd);
    }
}
