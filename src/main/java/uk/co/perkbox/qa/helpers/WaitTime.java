package uk.co.perkbox.qa.helpers;

/**
 * Helper class for waiting times
 */
public class WaitTime {

    public static final long ONE_SEC = 1;
    public static final long FIVE_SEC = 5;
    public static final long HALF_MIN = 30;
    public static final long ONE_MIN = 60;
    public static final long FIVE_MIN = 300;
    public static final long TEN_MIN = 600;
}
