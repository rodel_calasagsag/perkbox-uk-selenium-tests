package uk.co.perkbox.qa.pageobjects.deals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.co.perkbox.qa.pageobjects.LoginPage;

/**
 * Page object for the block containing each deal in the Deals Page
 */
public class DealBlock {

    // locators
    private By contentLocator = By.className("block__content");
    private By dealNameLocator = By.cssSelector("a h4");
    private By getThisPerkBtnLocator = By.className("btn--secondary");
    private By divWithIdLocator = By.className("block--expand--image--deal--small");

    // page elements
    private WebDriver driver;
    private WebElement content;
    private WebElement dealName;
    private WebElement getThisPerkBtn;
    private WebElement divWithId;

    DealBlock(WebDriver driver, WebElement parentBlock) {
        this.driver = driver;
        content = parentBlock.findElement(contentLocator);
        divWithId = parentBlock.findElement(divWithIdLocator);
        dealName = content.findElement(dealNameLocator);
        getThisPerkBtn = content.findElement(getThisPerkBtnLocator);
    }

    public String getPerkName() {
        return dealName.getText();
    }

    public LoginPage clickGetThisPerkBtn() {
        getThisPerkBtn.click();
        return new LoginPage(driver);
    }

    public String getId() {
        return divWithId.getAttribute("id");
    }
}
