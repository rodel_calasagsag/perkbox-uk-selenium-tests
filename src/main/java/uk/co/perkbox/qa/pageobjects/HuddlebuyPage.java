package uk.co.perkbox.qa.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import uk.co.perkbox.qa.helpers.Browser;
import uk.co.perkbox.qa.pageobjects.common.CookieConsentBanner;

/**
 * Page object for the Huddlebuy Page
 */
public class HuddlebuyPage {

    public static final String PAGE_TITLE = "Perkbox - Investing in employee happiness";
    private WebDriver driver;
    private Browser browser;

    // page elements
    @FindBy(id = "firstname")
    private WebElement firstNameField;
    @FindBy(id = "lastname")
    private WebElement lastNameField;
    @FindBy(id = "role")
    private WebElement jobTitleElem;
    @FindBy(id = "gender-0")
    private WebElement maleRadio;
    @FindBy(id = "gender-1")
    private WebElement femaleRadio;
    @FindBy(id = "date_of_birth-day")
    private WebElement bDayElem;
    @FindBy(id = "date_of_birth-month")
    private WebElement bMonthElem;
    @FindBy(id = "date_of_birth-year")
    private WebElement bYearElem;
    @FindBy(id = "email_address")
    private WebElement emailField;
    @FindBy(id = "mobile_phone")
    private WebElement mobileField;
    @FindBy(id = "secret_code")
    private WebElement secretCodeField;
    @FindBy(id = "saveEmployee")
    private WebElement submitBtn;
    @FindBy(className = "message-box--success")
    private WebElement successMsgBox;

    public HuddlebuyPage(WebDriver driver) {
        this.driver = driver;
        browser = new Browser(driver);
        PageFactory.initElements(driver, this);
        new CookieConsentBanner(driver).close();
    }

    public HuddlebuyPage typeFirstName(String firstName) {
        firstNameField.clear();
        firstNameField.sendKeys(firstName);
        return this;
    }

    public HuddlebuyPage typeLastName(String lastName) {
        lastNameField.clear();
        lastNameField.sendKeys(lastName);
        return this;
    }

    public HuddlebuyPage selectJobTitle(String jobTitle) {
        String elemClassName = jobTitleElem.getAttribute("class");
        Select jobTitleSelect = new Select(jobTitleElem);

        showElement(jobTitleElem);
        jobTitleSelect.selectByVisibleText(jobTitle);
        hideElement(jobTitleElem, elemClassName);
        return this;
    }

    private void hideElement(WebElement element, String className) {
        String cmd = "arguments[0].className='" + className + "';";

        browser.runScript(cmd, element);
    }

    /**
     * Run a javascript command that unhides an element by clearing the css class name
     *
     * @param elem {@link WebElement} to be unhidden
     */
    private void showElement(WebElement elem) {
        String cmd = "arguments[0].className='';";

        browser.runScript(cmd, elem);
    }

    public HuddlebuyPage selectGender(String gender) {
        String strMale = "Male";
        String strFemale = "Female";

        if (gender.equalsIgnoreCase(strMale)) {
            maleRadio.click();
        }
        if (gender.equalsIgnoreCase(strFemale)) {
            femaleRadio.click();
        }
        return this;
    }

    public HuddlebuyPage selectBirthDay(String day) {
        String className = bDayElem.getAttribute("class");
        Select daySelect = new Select(bDayElem);

        showElement(bDayElem);
        daySelect.selectByVisibleText(day);
        hideElement(bDayElem, className);
        return this;
    }

    public HuddlebuyPage selectBirthMonth(String month) {
        String className = bMonthElem.getAttribute("class");
        Select monthSelect = new Select(bMonthElem);

        showElement(bMonthElem);
        monthSelect.selectByVisibleText(month);
        hideElement(bMonthElem, className);
        return this;
    }

    public HuddlebuyPage selectBirthYear(String year) {
        String className = bYearElem.getAttribute("class");
        Select yearSelect = new Select(bYearElem);

        showElement(bYearElem);
        yearSelect.selectByVisibleText(year);
        hideElement(bYearElem, className);
        return this;
    }

    public HuddlebuyPage typeEmail(String email) {
        emailField.clear();
        emailField.sendKeys(email);
        return this;
    }

    public HuddlebuyPage typeMobile(String mobile) {
        mobileField.clear();
        mobileField.sendKeys(mobile);
        return this;
    }

    public HuddlebuyPage typeSecretCode(String secretCode) {
        secretCodeField.clear();
        secretCodeField.sendKeys(secretCode);
        return this;
    }

    public void clickSubmit() {
        submitBtn.click();
    }

    public boolean showsSignUpSuccessMessage() {
        String msgHeader = "We're currently processing your details...";
        String msgBody = "We'll have you united with your colleagues in a jiffy! The Perkbox minions will send you an email when your account is ready.";
        boolean showsSuccessMsgBox = successMsgBox.isDisplayed();
        boolean headerIsCorrect = successMsgBox.getText().contains(msgHeader);
        boolean bodyIsCorrect = successMsgBox.getText().contains(msgBody);

        return showsSuccessMsgBox && headerIsCorrect && bodyIsCorrect;
    }
}
