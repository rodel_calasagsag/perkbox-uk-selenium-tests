package uk.co.perkbox.qa.pageobjects.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import uk.co.perkbox.qa.helpers.Browser;

import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;
import static uk.co.perkbox.qa.helpers.WaitTime.FIVE_SEC;
import static uk.co.perkbox.qa.helpers.WaitTime.ONE_SEC;

/**
 * Page object for the cookie consent banner that appears in all pages
 */
public class CookieConsentBanner {

    // constants
    private static final String GOT_IT_BUTTON_LOCATION = ".cc_btn.cc_btn_accept_all";

    // page elements
    @FindBy(css = GOT_IT_BUTTON_LOCATION)
    private WebElement gotItBtn;

    // page objects
    private WebDriver driver;
    private Browser browser;

    public CookieConsentBanner(WebDriver driver) {
        this.driver = driver;
        browser = new Browser(driver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Check visibility of the "Got it" button, then click it to close the banner
     * to avoid blocking other page elements. Automatically proceed if the button wasn't visible.
     */
    public void close() {
        try {
            browser.waitFor(FIVE_SEC).until(visibilityOf(gotItBtn));
            gotItBtn.click();
            browser.waitFor(ONE_SEC).until(invisibilityOfElementLocated(By.cssSelector(GOT_IT_BUTTON_LOCATION)));
        } catch (Exception e) {
            // just log any possible exceptions caught. Do nothing really special here
            System.out.println(e.toString());
        }
    }
}
