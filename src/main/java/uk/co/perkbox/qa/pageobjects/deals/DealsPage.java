package uk.co.perkbox.qa.pageobjects.deals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import uk.co.perkbox.qa.helpers.Browser;
import uk.co.perkbox.qa.helpers.WaitTime;
import uk.co.perkbox.qa.pageobjects.common.CookieConsentBanner;

import java.util.List;

/**
 * Page object for the Deals Page
 */
public class DealsPage {

    // page constants
    public static final String TITLE = "Perkbox - Investing in employee happiness";
    private static final int BATCH_SIZE = 9;
    private static final String DEAL_BLOCKS_SELECTOR = "#deals-block > div";

    // page elements
    @FindBy(css = DEAL_BLOCKS_SELECTOR)
    private List<WebElement> dealBlocks;
    private By dealBlocksLocator = By.cssSelector(DEAL_BLOCKS_SELECTOR);

    // page objects
    private WebDriver driver;
    private Browser browser;
    private DealBlock dealBlock;

    public DealsPage(WebDriver driver) {
        this.driver = driver;
        browser = new Browser(driver);
        PageFactory.initElements(driver, this);
        new CookieConsentBanner(driver).close();
    }

    /**
     * Look for the desired perk by scrolling down, and then select it by clicking its "Get This Perk" button
     *
     * @param perkName Name of the perk
     */
    public DealBlock scrollToPerk(String perkName) {
        boolean found = false;

        do {
            int startIdx = dealBlocks.size() - BATCH_SIZE;

            for (int i = startIdx; i < dealBlocks.size(); i++) {
                dealBlock = new DealBlock(driver, dealBlocks.get(i));

                if (dealBlock.getPerkName().equalsIgnoreCase(perkName)) {
                    System.out.println("Found perk \"" + perkName + "\" at tile " + (i + 1));
                    found = true;
                    break;
                }
            }
            if (!found) {
                scrollToBottom();
            }
        } while (!found);
        return dealBlock;
    }

    private void scrollToBottom() {
        int initialBlockCount = dealBlocks.size();
        int heightMultiplier = initialBlockCount / BATCH_SIZE;
        String cmd = "scroll(0, window.outerHeight * " + heightMultiplier + ");";

        browser.runScript(cmd);
        browser.waitFor(WaitTime.HALF_MIN)
                .until(ExpectedConditions.numberOfElementsToBeMoreThan(dealBlocksLocator, initialBlockCount));
    }
}
