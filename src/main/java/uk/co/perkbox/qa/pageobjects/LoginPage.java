package uk.co.perkbox.qa.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import uk.co.perkbox.qa.helpers.Browser;
import uk.co.perkbox.qa.helpers.WaitTime;

/**
 * Page object for the Login page
 */
public class LoginPage {

    private WebDriver driver;
    private Browser browser;

    @FindBy(id = "login-form")
    private WebElement loginForm;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        browser = new Browser(driver);

        PageFactory.initElements(driver, this);
        browser.waitFor(WaitTime.FIVE_SEC).until(ExpectedConditions.visibilityOf(loginForm));
    }

    public boolean showsLoginForm() {
        return loginForm.isDisplayed();
    }
}
